<?php declare(strict_types=1);

/**
 * @package   Memo\ProductBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\ProductBundle\Service;



class LanguageService
{

	/**
	 * @param bool $bolIncludeCurrent
	 * @param false $bolOnlyActiveLanguages
	 * @return array
	 * @throws \Exception
	 */
	public static function getAllLanguages($bolIncludeCurrent=true, $bolOnlyActiveLanguages=false)
	{
		// Presets
		$strDefaultLanguage = self::getDefaultLanguage();
		$arrLanguages = array();

		if($bolOnlyActiveLanguages) {

			// Get all Languages by fetching Root-Pages (has to be published)
			$colRootPages = \PageModel::findBy(array('type=?', 'language!=?', 'published=?'), array('root', "", 1));
			$objRootPage = \PageModel::findOneBy(array('type=?', 'language!=?', 'fallback=?'), array('root', "", 1));

		} else {

			// Get all Languages by fetching Root-Pages (does not have to be published)
			$colRootPages = \PageModel::findBy(array('type=?', 'language!=?'), array('root', ""));
			$objRootPage = \PageModel::findOneBy(array('type=?', 'language!=?', 'fallback=?'), array('root', "", 1));

		}

		// Check if languages were defined in the page-tree
		if (!$objRootPage || !$colRootPages) {
			throw new \Exception("No root-page with a language or no fallback language defined");
		}

		// Add the languages to a array
		while ($colRootPages->next())
		{
			if(!in_array($colRootPages->languag, $arrLanguages)){
				$arrLanguages[$colRootPages->language] = $colRootPages->language;
			}
		}

		// Remove current language - if widhed for
		if(!$bolIncludeCurrent){
			foreach($arrLanguages as $intKey => $strLanguage)
			{
				if($strDefaultLanguage == $strLanguage)
				{
					unset($arrLanguages[$intKey]);
				}
			}

		}

		return $arrLanguages;
	}

	/**
	 * Generate DCA-Fields for translations
	 * @param $strTable
	 * @throws \Exception
	 */
	public function generateTranslateDCA($strTable)
	{
		$strFallbackLanguage = self::getDefaultLanguage();
		$arrLanguages = self::getAllLanguages();

		// Loop all Languages
		foreach($arrLanguages as $strLanguage) {

			// Reset the palette string
			unset($arrTranslatedFields);

			// Detect Default language and "jump" it
			if ($strLanguage == $strFallbackLanguage) {
				continue;
			}

			// Define Postfix and Language Values
			$strPostfix = "_" . $strLanguage;

			// Loop all DCA-fields
			foreach ($GLOBALS['TL_DCA'][$strTable]['fields'] as $strFieldName => $arrFieldValues) {

				// Detect fields, that where marked as "translate"
				if ($arrFieldValues['translate']) {

					unset($arrNewField);

					// Add the fieldname to the array of fieldnames
					$arrTranslatedFields[] = $strFieldName . $strPostfix;

					// Create a new field-array and clear all values, not needed in the translation
					$arrNewField = $arrFieldValues;
					$arrNewField['translate'] = false;
					$arrNewField['filter'] = false;
					$arrNewField['search'] = false;
					$arrNewField['sort'] = false;
					$arrNewField['eval']['mandatory'] = false;
					unset($arrNewField['save_callback']);

					// Enable the new language-fields to be manipulated before we return them to the array
					if (isset($GLOBALS['TL_HOOKS']['prepareTranslatedField']) && is_array($GLOBALS['TL_HOOKS']['prepareTranslatedField'])) {
						foreach ($GLOBALS['TL_HOOKS']['prepareTranslatedField'] as $callback) {
							$this->import($callback[0]);
							$arrNewField = $this->$callback[0]->$callback[1]($arrNewField, $arrFieldValues, $strPostfix, $strTable);
						}
					}

					// Save the new field to the array of fields
					$GLOBALS['TL_DCA'][$strTable]['fields'][$strFieldName . $strPostfix] = $arrNewField;

				}

			}

			// Build the palette string for the new language
			$strNewPalette = '{translation' . $strPostfix . ':hide}, ';
			if(is_array($arrTranslatedFields)){
				$strNewPalette .= implode(', ', $arrTranslatedFields);
			}
			$strNewPalette .= ';';


			// Add the new palette-string to the existing palettes
			$bolPaletteFound = false;
			foreach ($GLOBALS['TL_DCA'][$strTable]['palettes'] as $strPaletteName => $strPalette) {

				if ($strPaletteName != '__selector__' && stristr($GLOBALS['TL_DCA'][$strTable]['palettes'][$strPaletteName], '{publish_legend}')) {
					$bolPaletteFound = true;
					$GLOBALS['TL_DCA'][$strTable]['palettes'][$strPaletteName] = str_replace('{publish_legend}', $strNewPalette . '{publish_legend}', $GLOBALS['TL_DCA'][$strTable]['palettes'][$strPaletteName]);
				}
			}

			if(!$bolPaletteFound)
			{
				throw new \Exception('No "publish_legend" found in the palette for the table: ' . $strTable);
			}

			// Add the legend.
			$GLOBALS['TL_LANG'][$strTable]['translation' . $strPostfix] = 'Übersetzungen ' . strtoupper($strLanguage);
		}
	}


	/**
	 * Returns the cleaned postfix string, that is added to dca-fields for multilanguage content
	 * @param    $strLanguage    Language (de, de_CH, en, fr, etc)
	 * @return $strPostfix        Language-Postfix (de, dech, en, fr, etc.)
	 **/
	public static function getLanguagePostfix(string $strLanguage)
	{
		// Get Default Language
		$strDefaultLanguage = self::getDefaultLanguage();

		// Cleanup Language-String
		$strPostfix = strtolower($strLanguage);
		$strPostfix = str_replace('_', '', $strPostfix);
		$strPostfix = str_replace('-', '', $strPostfix);
		$strPostfix = str_replace(' ', '', $strPostfix);

		// Detect if the Default-Language is wanted
		if ($strPostfix == $strDefaultLanguage) {
			$strPostfix = '';
		} else {
			$strPostfix = '_' . $strPostfix;
		}

		// Return the postfix-string
		return $strPostfix;
	}

	/**
	 * @return mixed|string|null
	 */
	public static function getDefaultLanguage()
	{
		$strDefaultLanguage = \Contao\Config::get('default_language');

		if(!$strDefaultLanguage){
			$strDefaultLanguage = 'de';
		}

		return $strDefaultLanguage;
	}

    /**
     * @param ProductModel $ResultSet
     * @return ProductModel Result Set
     */
    public static function translateDCA($ResultSet,$strTable) {
        if(empty($ResultSet) OR empty($strTable)) {
            return $ResultSet;
        }

        //Translate Fields if needet.
        \Controller::loadDataContainer($strTable);
        // Get Language Postfix
        $cLang = $GLOBALS['TL_LANGUAGE'];
        $strPostfix = self::getLanguagePostfix($cLang);

        //Get Translated Fields
        $aFields        = ($GLOBALS['TL_DCA'][$strTable]['fields']);
        $aTransFields   = [];
        if($aFields) {
            foreach ($aFields as $key => $val) {
                if ($val['translate'] == true) {
                    $aTransFields[$key] = $val;
                }
            }
        }

        //Translate Fields
        //loop rows
        foreach($ResultSet as $key => $row) {
            //loop Fields
            foreach($aTransFields as $field => $fData) {
                $langField = $field.$strPostfix;
                if(!empty($row->$langField)) {
                    //Default Field Value
                    $ResultSet[$key]->$field = $row->$langField;
                }
            }
        }
        return $ResultSet;
    }

}
