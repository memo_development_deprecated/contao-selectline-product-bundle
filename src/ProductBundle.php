<?php

namespace Memo\ProductBundle;

/**
 * @package   Product Bundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ProductBundle extends Bundle
{

}
