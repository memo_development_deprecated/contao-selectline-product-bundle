<?php

declare(strict_types=1);

/**
 * @package   SelectLine Product Bundle
 * @author    Media Motion AG, Christian Nussbaumer
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\ProductBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\RouteCollection;
use Memo\ProductBundle\ProductBundle;

/**
 * @internal
 */
class Plugin implements BundlePluginInterface, RoutingPluginInterface
{
	public function getBundles(ParserInterface $parser): array
	{
		return [
			BundleConfig::create(ProductBundle::class)
				->setLoadAfter([ContaoCoreBundle::class]),
		];
	}

	public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel): RouteCollection
	{
		return $resolver
			->resolve(__DIR__.'/../Resources/config/routes.yml')
			->load(__DIR__.'/../Resources/config/routes.yml')
			;
	}
}
