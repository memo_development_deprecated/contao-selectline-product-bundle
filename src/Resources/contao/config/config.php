<?php declare(strict_types=1);

/**
 * @package   Selectline Product Bundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
array_insert($GLOBALS['BE_MOD']['memo_productbundle'], 100, array
(
	'memoProducts' => array
	(
		'tables'       => array(
			'tl_memo_products',
			'tl_memo_products_function',
			'tl_memo_products_license',
		)
	),
	'memoProductLicense' => [
		'tables'	=> [
			'tl_memo_products_license',
			'tl_memo_products',
			'tl_memo_products_function',
		]
	]
));

/**
 * Add front end modules and Custom Elements
 */
$GLOBALS['FE_MOD']['memoProducts']['memo_products_list']      = 'Memo\ProductBundle\Module\ModuleProductListing';


/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_memo_products']   		= 'Memo\ProductBundle\Model\ProductModel';
$GLOBALS['TL_MODELS']['tl_memo_products_function']  = 'Memo\ProductBundle\Model\ProductFunctionModel';
$GLOBALS['TL_MODELS']['tl_memo_products_license']   = 'Memo\ProductBundle\Model\ProductLicenseModel';


/**
 * Backend CSS
 */
if(TL_MODE == 'BE')
{
	$GLOBALS['TL_CSS'][]        = '/bundles/product/css/be_product_bundle.css';
}
