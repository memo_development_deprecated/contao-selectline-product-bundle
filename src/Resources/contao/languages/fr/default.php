<?php


$GLOBALS['TL_LANG']['MSC']['freeTrial'] = "Testez maintenant gratuitement";
$GLOBALS['TL_LANG']['MSC']['priceTaxInfo'] = "Prix en CHF, TVA non comprise";
$GLOBALS['TL_LANG']['MSC']['annual'] = "Annuellement";
$GLOBALS['TL_LANG']['MSC']['monthly'] = "Mensuel";
$GLOBALS['TL_LANG']['MSC']['buyNow'] = "Commander";
$GLOBALS['TL_LANG']['MSC']['features'] = "Fonctions";
$GLOBALS['TL_LANG']['MSC']['buy'] = "Achetez";
$GLOBALS['TL_LANG']['MSC']['rent'] = "Loyer";

