<?php


/**
 * Contao Open Source CMS

 * @package   ProductBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */



$GLOBALS['TL_LANG']['tl_memo_products_function']['protected_legend']   	= 'Zugriffsschutz';
$GLOBALS['TL_LANG']['tl_memo_products_function']['protected']   	    = ['Zugriffsschutz'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['title_legend']   		= 'Funktion';
$GLOBALS['TL_LANG']['tl_memo_products_function']['expert_legend']   	= 'Experten-Einstellungen';
$GLOBALS['TL_LANG']['tl_memo_products_function']['publish_legend']   	= 'Veröffentlichung';
$GLOBALS['TL_LANG']['tl_memo_products_function']['license']   	        = ['Lizenz'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['all']   				= ['Mehrere bearbeiten'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['sortOrder']			= ['individuelle Sortierung'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['new']   				= ['neuer Eintrag'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['title']  				= ['Titel'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['text']   				= ['Text'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['guests']              = ['Nur Gästen anzeigen','Dieses Produkt verstecken, sobald ein Mitglied angemeldet ist.'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['cssID']               = ['CSS-ID/Klasse','Hier können Sie eine ID und beliebig viele Klassen eingeben'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['published']           = ['Funktion veröffentlichen','Das Produkt auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['start']               = ['Anzeigen ab','Den Artikel erst ab diesem Tag auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_memo_products_function']['stop']                 = ['Anzeigen bis','Den Artikel nur bis zu diesem Tag auf der Webseite anzeigen.'];
