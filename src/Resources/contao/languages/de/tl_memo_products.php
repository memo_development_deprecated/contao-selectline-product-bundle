<?php


/**
 * Contao Open Source CMS

 * @package   ProductBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */



$GLOBALS['TL_LANG']['tl_memo_products']['protected_legend'] = 'Zugriffsschutz';
$GLOBALS['TL_LANG']['tl_memo_products']['title_legend']     = 'Produkt';
$GLOBALS['TL_LANG']['tl_memo_products']['all']              = ['Mehrere bearbeiten'];
$GLOBALS['TL_LANG']['tl_memo_products']['new']              = ['neuer Eintrag'];
$GLOBALS['TL_LANG']['tl_memo_products']['publish_legend']   = 'Veröffentlichung';



$GLOBALS['TL_LANG']['tl_memo_products']['title']   	    = ['Titel',""];
$GLOBALS['TL_LANG']['tl_memo_products']['text']		    = ['Beschreibung',""];
$GLOBALS['TL_LANG']['tl_memo_products']['sortOrder']	= ['manuelle Sortierung',""];
$GLOBALS['TL_LANG']['tl_memo_products']['alias']		= ['Alias',""];
$GLOBALS['TL_LANG']['tl_memo_products']['groups']       = ['Produkt schützen',"Inhalte nur bestimmten Mitgliedergruppen anzeigen?"];
$GLOBALS['TL_LANG']['tl_memo_products']['protected']    = ['Erlaubte Mitgliedergruppen',""];
$GLOBALS['TL_LANG']['tl_memo_products']['published']    = ['Produkt veröffentlichen','Das Produkt auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_memo_products']['start']        = ['Anzeigen ab','Den Artikel erst ab diesem Tag auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_memo_products']['stop']         = ['Anzeigen bis','Den Artikel nur bis zu diesem Tag auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_memo_products']['product']      = ['Produkte','Die hier selektierten Produkte werden ausgegeben. Standardmässig werden alle Produkte ausgegeben.'];
$GLOBALS['TL_LANG']['tl_memo_products']['linkTitle']    = ['Link Titel',""];
