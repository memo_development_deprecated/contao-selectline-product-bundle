<?php

$GLOBALS['TL_LANG']['MSC']['freeTrial'] = "kostenlos testen";
$GLOBALS['TL_LANG']['MSC']['priceTaxInfo'] = "Preise in CHF, exkl. MWST";
$GLOBALS['TL_LANG']['MSC']['annual'] = "Jährlich";
$GLOBALS['TL_LANG']['MSC']['monthly'] = "Monatlich";
$GLOBALS['TL_LANG']['MSC']['buyNow'] = "Jetzt kaufen";
$GLOBALS['TL_LANG']['MSC']['features'] = "Funktionen";
$GLOBALS['TL_LANG']['MSC']['buy'] = "Kauf";
$GLOBALS['TL_LANG']['MSC']['rent'] = "Miete";