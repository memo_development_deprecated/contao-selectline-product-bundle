<?php
/**
 * Contao Open Source CMS
 *
 * @package   ProductBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['memo_productbundle'] = ['Produkte verwalten',''];
$GLOBALS['TL_LANG']['MOD']['memoProducts'] = ['Produkt Übersicht',''];
$GLOBALS['TL_LANG']['MOD']['memoProductLicense'] = ['Lizenz Übersicht',''];

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['memoProducts'] = ['MEMO Produkte',''];
$GLOBALS['TL_LANG']['FMD']['memo_product_list'] = ['Produkt Listing',''];

