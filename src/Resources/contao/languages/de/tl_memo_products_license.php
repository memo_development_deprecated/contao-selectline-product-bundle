<?php


/**
 * Contao Open Source CMS

 * @package   ProductBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */



$GLOBALS['TL_LANG']['tl_memo_products_license']['protected_legend'] = 'Zugriffsschutz';
$GLOBALS['TL_LANG']['tl_memo_products_license']['title_legend']   	= 'Lizenz';
$GLOBALS['TL_LANG']['tl_memo_products_license']['expert_legend']   	= 'Experten-Einstellungen';
$GLOBALS['TL_LANG']['tl_memo_products_license']['publish_legend']   	= 'Veröffentlichung';
$GLOBALS['TL_LANG']['tl_memo_products_license']['attribute_legend'] = 'Attribute';
$GLOBALS['TL_LANG']['tl_memo_products_license']['all']   			= ['Mehrere bearbeiten'];
$GLOBALS['TL_LANG']['tl_memo_products_license']['new']   			= ['neuer Eintrag'];



$GLOBALS['TL_LANG']['tl_memo_products_license']['title']   	    = ['Titel',""];
$GLOBALS['TL_LANG']['tl_memo_products_license']['text']		    = ['Beschreibung',""];
$GLOBALS['TL_LANG']['tl_memo_products_license']['sortOrder']    = ['individuelle Sortierung',""];
$GLOBALS['TL_LANG']['tl_memo_products_license']['product_fk']		    = ['Produkt',"Verknüpfung mit dem Produkt"];
$GLOBALS['TL_LANG']['tl_memo_products_license']['alias']	    = ['Alias',""];
$GLOBALS['TL_LANG']['tl_memo_products_license']['groups']       = ['Produkt schützen',"Inhalte nur bestimmten Mitgliedergruppen anzeigen?"];
$GLOBALS['TL_LANG']['tl_memo_products_license']['protected']    = ['Erlaubte Mitgliedergruppen',""];
$GLOBALS['TL_LANG']['tl_memo_products_license']['price_month']  = ['Mietpreis pro Monat',"z.Bsp: nur 190.-"];
$GLOBALS['TL_LANG']['tl_memo_products_license']['price_year']   = ['Kaufpreis einmalig',"z.Bsp: nur 2'000.-"];
$GLOBALS['TL_LANG']['tl_memo_products_license']['user_info']    = ['Anzahl User Miete',"z.Bsp: Max. 1 User"];
$GLOBALS['TL_LANG']['tl_memo_products_license']['user_info_buy']    = ['Anzahl User Kauf',"z.Bsp: Max. 1 User"];
$GLOBALS['TL_LANG']['tl_memo_products_license']['price_info']    = ['Preis Info',"z.Bsp: Preise in CHF, exkl. MWST"];
$GLOBALS['TL_LANG']['tl_memo_products_license']['linkTitle']    = ['Link Titel',""];

$GLOBALS['TL_LANG']['tl_memo_products_license']['cssID']               = ['CSS-ID/Klasse','Hier können Sie eine ID und beliebig viele Klassen eingeben'];
$GLOBALS['TL_LANG']['tl_memo_products_license']['published']           = ['Produkt veröffentlichen','Das Produkt auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_memo_products_license']['start']               = ['Anzeigen ab','Den Artikel erst ab diesem Tag auf der Webseite anzeigen.'];
$GLOBALS['TL_LANG']['tl_memo_products_license']['stop']                = ['Anzeigen bis','Den Artikel nur bis zu diesem Tag auf der Webseite anzeigen.'];
