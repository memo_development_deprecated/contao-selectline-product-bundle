<?php
$GLOBALS['TL_LANG']['MSC']['freeTrial'] = "Prova ora gratis";
$GLOBALS['TL_LANG']['MSC']['priceTaxInfo'] = "Prezzi in CHF, IVA esclusa";
$GLOBALS['TL_LANG']['MSC']['annual'] = "Annuale";
$GLOBALS['TL_LANG']['MSC']['monthly'] = "Mensile";
$GLOBALS['TL_LANG']['MSC']['buyNow'] = "ordinare";
$GLOBALS['TL_LANG']['MSC']['features'] = "Funzioni";
$GLOBALS['TL_LANG']['MSC']['buy'] = "Compra";
$GLOBALS['TL_LANG']['MSC']['rent'] = "Affitto";
