<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   ProductBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\DataContainer;
use Memo\ProductBundle\Service\LanguageService;

$GLOBALS['TL_DCA']['tl_memo_products_function'] = array
(
	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'ptable'                      => 'tl_memo_products',
		'switchToEdit'                => true,
		'enableVersioning'            => true,
        'markAsCopy'                  => 'title',
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
                'pid,sorting,start,stop,published' => 'index'
			)
		)
	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 4,
			'fields'                  => array('sorting'),
			'panelLayout'             => 'filter;search,limit',
            'headerFields'            => ['id','title'],
            'disableGrouping'         => true,
            'child_record_callback'   => array('tl_memo_products_function', 'addChildRecords'),
            'child_record_class'      => 'no_padding'
		),
		'label' => array
		(
			'fields'                  => array('title'),
			'showColumns'             => false,
            'format'                  => '%s'
		),
		'global_operations' => array
		(
			'all' => array
			(
			    'label'               => $GLOBALS['TL_LANG']['tl_memo_products_function']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'href'                => 'act=edit',
				'icon'                => 'edit.svg'
			),
			'copy' => array
			(
				'href'                => 'act=copy',
				'icon'                => 'copy.svg'
			),
			'delete' => array
			(
				'href'                => 'act=delete',
				'icon'                => 'delete.svg',
				'attributes'          => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
            'toggle' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_memo_products_function']['toggle'],
                'icon'                => 'visible.svg',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_memo_products_function', 'toggleIcon')
            ),
            'show' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_memo_products_function']['show'],
                'href'                => 'act=show',
                'icon'                => 'show.svg'
            )
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'                => array('protected', 'allowComments'),
		'default'                     => '{title_legend},title,text,license;{publish_legend},published,start,stop;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID;'
	),

	// Subpalettes
	'subpalettes' => array
	(
		'protected'                   => 'groups'
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'              => 'tl_memo_products.id',
			'sql'                     => "int(10) unsigned NOT NULL default 0",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default 0"
		),
		'title' => array
		(
			'label' 				  => $GLOBALS['TL_LANG']['tl_memo_products_function']['title'],
            'translate'				  => true,
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'text' => array
		(
			'label' 				  => $GLOBALS['TL_LANG']['tl_memo_products_function']['text'],
            'translate'				  => true,
			'exclude'                 => true,
			'search'                  => true,
			'inputType'               => 'textarea',
			'eval'                    => array(
				'mandatory'=>false,
				'rte'=>'tinyMCE',
				'helpwizard'=>true,
				'tl_class'=>'clr'
			),
			'explanation'             => 'insertTags',
			'sql'                     => "mediumtext NULL"
		),

		'protected' => array
		(
			'label' 				  => $GLOBALS['TL_LANG']['tl_memo_products_function']['protected'],
			'exclude'                 => true,
			'filter'                  => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('submitOnChange'=>true),
			'sql'                     => "char(1) NOT NULL default ''"
		),
		'groups' => array
		(
			'label' 				  => $GLOBALS['TL_LANG']['tl_memo_products_function']['groups'],
			'exclude'                 => true,
			'inputType'               => 'checkbox',
			'foreignKey'              => 'tl_member_group.name',
			'eval'                    => array('mandatory'=>true, 'multiple'=>true),
			'sql'                     => "blob NULL",
			'relation'                => array('type'=>'hasMany', 'load'=>'lazy')
		),
		'license' => array
		(
			'label' 				  => $GLOBALS['TL_LANG']['tl_memo_products_function']['license'],
			'exclude' 					=> true,
			'search'					=> false,
			'filter'					=> false,
			'inputType'					=> 'checkbox',
            'options_callback'          => ['tl_memo_products_function','getLicenses'],
			'eval'						=> array(
				'multiple' => true,
				'chosen' => true,
				'includeBlankOption' => true,
				'tl_class' => 'clr long'
			),
			'sql'						=> "blob NULL"
		),

		'sorting' => array
		(
            'label'                   => &$GLOBALS['TL_LANG']['tl_memo_products_function']['sortOrder'],
			'sorting'                 => true,
			'exclude'                 => true,
			'inputType'               => 'text',
			'flag'                    => 11,
			'sql'                     => "int(10) default 0"
		),
        'guests' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_memo_products_function']['guests'],
            'exclude'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'cssID' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_memo_products_function']['cssID'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('multiple'=>true, 'size'=>2, 'tl_class'=>'w50 clr'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'published' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_memo_products_function']['published'],
            'exclude'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('doNotCopy'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'start' => array
        (
            'exclude'                 => true,
            'label'                   => &$GLOBALS['TL_LANG']['tl_memo_products_function']['start'],
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'exclude'                 => true,
            'label'                   => &$GLOBALS['TL_LANG']['tl_memo_products_function']['stop'],
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'datim', 'datepicker'=>true, 'tl_class'=>'w50 wizard'),
            'sql'                     => "varchar(10) NOT NULL default ''"
        )

	)
);

// Add multi-lang fields automatically by checking for the translate field
$objLanguageService = \System::getContainer()->get('memo.ProductBundle.language');
$objLanguageService->generateTranslateDCA('tl_memo_products_function');


use \Memo\ProductBundle\Model\ProductLicenseModel;
use \Memo\ProductBundle\Model\ProductFunctionModel;

class tl_memo_products_function extends Backend {

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('Contao\BackendUser', 'User');
	}

    /**
     * @param DataContainer $dc
     * @return array
     */
    public function getLicenses(DataContainer $dc) {
        $aReturn = [];
        $oLicenses = ProductLicenseModel::findBy('product_fk',$dc->activeRecord->pid);

        if($oLicenses) {
            foreach ($oLicenses as $key => $val) {
                $aReturn[$val->id] = $val->title;
            }
        }
        return $aReturn;
    }


    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid')))
        {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->hasAccess('tl_memo_products_function::published', 'alexf'))
        {
            return '';
        }

        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);

        if (!$row['published'])
        {
            $icon = 'invisible.svg';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }

    /**
     * Disable/enable a user group
     *
     * @param integer       $intId
     * @param boolean       $blnVisible
     * @param DataContainer $dc
     *
     * @throws Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }

        // Trigger the onload_callback
        if (is_array($GLOBALS['TL_DCA']['tl_memo_products_function']['config']['onload_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_memo_products_function']['config']['onload_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }

        // Check the field access
        if (!$this->User->hasAccess('tl_memo_products_function::published', 'alexf'))
        {
            throw new Contao\CoreBundle\Exception\AccessDeniedException('Not enough permissions to publish/unpublish Product ID "' . $intId . '".');
        }

        // Set the current record
        if ($dc)
        {
            $objRow = $this->Database->prepare("SELECT * FROM tl_memo_products_function WHERE id=?")
                ->limit(1)
                ->execute($intId);

            if ($objRow->numRows)
            {
                $dc->activeRecord = $objRow;
            }
        }

        $objVersions = new Versions('tl_memo_products_function', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_memo_products_function']['fields']['published']['save_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_memo_products_function']['fields']['published']['save_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
                }
                elseif (is_callable($callback))
                {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        $this->Database->prepare("UPDATE tl_memo_products_function SET tstamp=$time, published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);

        if ($dc)
        {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (is_array($GLOBALS['TL_DCA']['tl_memo_products_function']['config']['onsubmit_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_memo_products_function']['config']['onsubmit_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();
    }

    public function addChildRecords($row) {

        $sLicense = '';
        $aLicense = ProductLicenseModel::findMultipleByIds(unserialize($row['license']));

        if($aLicense) {
            foreach($aLicense as $key => $val) {
                $sLicense .= $val->title.", ";
            }
            $sLicense = substr($sLicense,0,-2);
        }

        return sprintf('<span class="">%s</span><span style="color:#999;padding-left:3px">[%s]</span>',$row['title'],$sLicense);
    }
}
