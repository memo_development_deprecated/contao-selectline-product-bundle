<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_products_list'] = '{title_legend}, name,type,headline;{product_legend},memo_product_license;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['memo_product_license'] = array
(
    'label' 				  => $GLOBALS['TL_LANG']['tl_module']['memo_product'],
    'exclude' 					=> true,
    'search'					=> false,
    'inputType'					=> 'select',
    'options_callback'          => ['memo_product_list_module_ext','getProducts'],
    'eval'						=> array(
        'multiple' => true,
        'chosen' => true,
        'includeBlankOption' => true,
        'tl_class' => 'clr long'
    ),
    'sql'						=> "blob NULL"
);

use \Memo\ProductBundle\Model\ProductModel;

class memo_product_list_module_ext extends Backend
{
    public function getProducts($dc) {
        $aReturn    = [];
        $oProducts  = ProductModel::findAll();
        if(empty($oProducts)) {
            return [];
        }

        foreach($oProducts as $key => $val) {
            $aReturn[$val->id] = $val->title;
        }
        return $aReturn;
    }
}
