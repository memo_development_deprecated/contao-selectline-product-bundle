<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   YellowPageBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\ProductBundle\Module;




use Memo\ProductBundle\Model\ProductFunctionModel;
use Memo\ProductBundle\Model\ProductLicenseModel;
use Memo\ProductBundle\Model\ProductModel;
use Memo\ProductBundle\ProductBundle;

class ModuleProductListing extends \Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_memo_products_list';

    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die <a href="/contao?do=tl_memo_products"><strong>hier verwalteten Inhalte</strong></a>.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }

    protected function compile()
    {
        if(empty($this->memo_product_license)) {
            $oData = ProductModel::findBy(['published=?'], [1], ['order' => 'sortOrder,title']);
        }else{

            $oData = ProductModel::findBy(['published=?','id IN ('.implode(',', array_map('\intval',unserialize($this->memo_product_license))).')'], [1], ['order' => 'sortOrder,title']);
        }

        //Add Function Items & Licenses
        foreach($oData as $key => $val) {
            //Add Functions
            $oData[$key]->functions = ProductFunctionModel::findBy(['pid=?','published=?'],[$val->id,1],['order'=>'license DESC, sorting, title']);

            //Add Licenses
            $oLicenses = ProductLicenseModel::findBy(['product_fk=?','published=?'],[$val->id,1],['order'=>'sortOrder, title']);
            $oLicenses->functions = ProductFunctionModel::findAllFunctionsByLicense(1,$val->id);
            $oData[$key]->licenses = $oLicenses;
        }

        $this->Template->oData = $oData;
    }
}
