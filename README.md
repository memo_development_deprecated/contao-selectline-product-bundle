# SelectLine Product Bundle

## About
Erfassung mehrsprachiger Inhalte für Produkte, Funktionen und Lizenzen.

### Features
- [x] Mehrsprachige Erfassung von Produkten und Funktionen
- [x] Mehrsprachige Erfassung von Lizenzen
- [x] Frontend Modul zur Ausgabe aller Produkte/Funktionen und Lizenzen


## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-selectline-product-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-selectline-product-bundle": "dev-master",
```

## Usage (German)


### Folgende Inserttags stehen zur Verfügung


## Contribution
Bug reports and pull requests are welcome
